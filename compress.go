// Compress provides a uniform way of working with compression formats.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compress

import (
	"errors"
	"io"
	"strconv"
)

// Type indicates the compression format, as implemented in another package.
type Type uint8

// The supported compression formats.
const (
	None   Type = iota
	Snappy      // import _ "bitbucket.org/pcastools/compress/snappy"
	Gzip        // import _ "bitbucket.org/pcastools/compress/gzip"
	Zlib        // import _ "bitbucket.org/pcastools/compress/zlib"
	Zstd        // import _ "bitbucket.org/pcastools/compress/zstd"
	S2          // import _ "bitbucket.org/pcastools/compress/s2"
	maxType
)

// Common errors.
var (
	ErrNotAvailable = errors.New("Compression type is unavailable (not linked into the binary)")
)

// The slices of reader and writer creation functions.
var (
	readerFuncs = make([]func(io.Reader) (io.ReadCloser, error), maxType)
	writerFuncs = make([]func(io.Writer) (io.WriteCloser, error), maxType)
)

/////////////////////////////////////////////////////////////////////////
// Type functions
/////////////////////////////////////////////////////////////////////////

// Available reports whether the given compression type is linked into the binary.
func (t Type) Available() bool {
	return t < maxType && readerFuncs[t] != nil
}

// NewReader creates a new reader for this compression type, reading from the given io.Reader r. It is the caller's responsibility to call Close on the reader when done.
func (t Type) NewReader(r io.Reader) (io.ReadCloser, error) {
	if t < maxType {
		if f := readerFuncs[t]; f != nil {
			return f(r)
		}
	}
	return nil, ErrNotAvailable
}

// NewWriter creates a new writer for this compression type. Writes to the returned writer will be compressed and written to w. It is the caller's responsibility to call Close on the writer when done.
func (t Type) NewWriter(w io.Writer) (io.WriteCloser, error) {
	if t < maxType {
		if f := writerFuncs[t]; f != nil {
			return f(w)
		}
	}
	return nil, ErrNotAvailable
}

// String returns a string description of this compression type.
func (t Type) String() string {
	switch t {
	case None:
		return "none"
	case Snappy:
		return "snappy"
	case Gzip:
		return "gzip"
	case Zlib:
		return "zlib"
	case S2:
		return "s2"
	}
	return "unknown [" + strconv.Itoa(int(t)) + "]"
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// Register registers the given compression type. Intended to be called from the compression type's init method.
func Register(t Type, rf func(io.Reader) (io.ReadCloser, error), wf func(io.Writer) (io.WriteCloser, error)) {
	if t >= maxType {
		panic("compress: Register of unknown compression type")
	} else if rf == nil {
		panic("compress: Register of nil reader function for compression type " + t.String())
	} else if wf == nil {
		panic("compress: Register of nil writer function for compression type " + t.String())
	}
	readerFuncs[t] = rf
	writerFuncs[t] = wf
}
