// None_test provides tests for the None compressor.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compress

import (
	"bytes"
	"fmt"
	"io"
	"testing"
)

func TestNone(t *testing.T) {
	// build a compressor w and decompressor r, backed by a buffer buf
	var buf bytes.Buffer
	w, err := None.NewWriter(&buf)
	defer w.Close()
	if err != nil {
		t.Errorf("Error on writer: %v\n", err)
	}
	r, err := None.NewReader(&buf)
	if err != nil {
		t.Errorf("Error on reader: %v\n", err)
	}
	// write to the writer, and then read from the reader
	// this should compress and then decompress
	var result bytes.Buffer
	io.WriteString(w, "Hello world.")
	_, err = result.ReadFrom(r)
	if err != nil {
		t.Errorf("Error on decode: %v\n", err)
	}
	fmt.Printf("%s\n", result.String())
}
