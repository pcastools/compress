// Gzip provides a access to the Gzip compression format from within package compress.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package gzip

import (
	"bitbucket.org/pcastools/compress"
	"github.com/klauspost/pgzip"
	"io"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the Gzip compression format.
func init() {
	compress.Register(
		compress.Gzip,
		func(r io.Reader) (io.ReadCloser, error) {
			return pgzip.NewReader(r)
		},
		func(w io.Writer) (io.WriteCloser, error) {
			return pgzip.NewWriter(w), nil
		},
	)
}
