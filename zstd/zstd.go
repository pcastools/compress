// Zstd provides a access to the Zstd compression format from within package compress.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package zstd

import (
	"bitbucket.org/pcastools/compress"
	"github.com/klauspost/compress/zstd"
	"io"
)

// reader wraps a *zstd.Decoder and provides a no-op Close method.
type reader struct {
	*zstd.Decoder
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the Snappy compression format.
func init() {
	compress.Register(
		compress.Zstd,
		func(r io.Reader) (io.ReadCloser, error) {
			d, err := zstd.NewReader(r)
			if err != nil {
				return nil, err
			}
			return &reader{d}, nil
		},
		func(w io.Writer) (io.WriteCloser, error) {
			return zstd.NewWriter(w)
		},
	)
}

/////////////////////////////////////////////////////////////////////////
// reader functions
/////////////////////////////////////////////////////////////////////////

// Close is a no-op and always returns nil.
func (r *reader) Close() error {
	r.Decoder.Close()
	return nil
}
