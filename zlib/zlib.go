// Zlib provides a access to the Zlib compression format from within package compress.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package zlib

import (
	"bitbucket.org/pcastools/compress"
	"compress/zlib"
	"io"
)

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the Snappy compression format.
func init() {
	compress.Register(
		compress.Zlib,
		zlib.NewReader,
		func(w io.Writer) (io.WriteCloser, error) {
			return zlib.NewWriter(w), nil
		},
	)
}
