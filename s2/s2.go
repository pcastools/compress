// s2 provides access to the S2 compression format from within package compress.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package s2

import (
	"bitbucket.org/pcastools/compress"
	"github.com/klauspost/compress/s2"
	"io"
)

// reader wraps an *s2.Reader and provides a no-op Close method.
type reader struct {
	*s2.Reader
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the S2 compression format.
func init() {
	compress.Register(
		compress.S2,
		func(r io.Reader) (io.ReadCloser, error) {
			return &reader{s2.NewReader(r)}, nil
		},
		func(w io.Writer) (io.WriteCloser, error) {
			return s2.NewWriter(w), nil
		},
	)
}

/////////////////////////////////////////////////////////////////////////
// reader functions
/////////////////////////////////////////////////////////////////////////

// Close is a no-op and always returns nil.
func (*reader) Close() error {
	return nil
}
