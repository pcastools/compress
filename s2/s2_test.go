// S2_test provides tests for the S2 compressor.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package s2

import (
	"bitbucket.org/pcastools/compress"
	"bitbucket.org/pcastools/compress/internal/compresstest"
	"testing"
)

func TestS2(t *testing.T) {
	compresstest.TestType(compress.S2, t)
}
