module bitbucket.org/pcastools/compress

go 1.13

require (
	github.com/golang/snappy v0.0.4
	github.com/klauspost/compress v1.16.4
	github.com/klauspost/pgzip v1.2.5
)
