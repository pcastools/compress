// Drivertest provides tests for implementations of driver.Interface.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compresstest

import (
	"bitbucket.org/pcastools/compress"
	"bytes"
	"crypto/rand"
	"io"
	"testing"
)

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

func TestType(c compress.Type, t *testing.T) {
	// Build a compressor w backed by a buffer buf
	var buf bytes.Buffer
	w, err := c.NewWriter(&buf)
	if err != nil {
		t.Errorf("error creating writer: %v", err)
	}
	// Write 5MB of random bytes to the writer
	const size = 5 * 1024 * 2014
	var data bytes.Buffer
	n, err := io.CopyN(w, io.TeeReader(rand.Reader, &data), size)
	if err != nil {
		t.Errorf("error on write: %v", err)
	} else if n != size {
		t.Errorf("wrote %d bytes but expected to write %d bytes", n, size)
	}
	// Close the writer
	if err = w.Close(); err != nil {
		t.Errorf("error closing writer: %v", err)
	}
	// Create a decompressor r
	r, err := c.NewReader(&buf)
	if err != nil {
		t.Errorf("error creating reader: %v", err)
	}
	// Read in the data from the reader
	var result bytes.Buffer
	n, err = result.ReadFrom(r)
	if err != nil {
		t.Errorf("error on read: %v\n", err)
	} else if n != size {
		t.Errorf("wrote %d bytes but read %d bytes", size, n)
	}
	// Close the reader
	if err = r.Close(); err != nil {
		t.Errorf("error closing reader: %v", err)
	}
	// Check that the data read agrees with the data written
	if !bytes.Equal(data.Bytes(), result.Bytes()) {
		t.Error("data written does not equal with data read")
	}
}
