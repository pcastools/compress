// Snappy provides a access to the Snappy compression format from within package compress.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package snappy

import (
	"bitbucket.org/pcastools/compress"
	"github.com/golang/snappy"
	"io"
)

// reader wraps a *snappy.Reader and provides a no-op Close method.
type reader struct {
	*snappy.Reader
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the Snappy compression format.
func init() {
	compress.Register(
		compress.Snappy,
		func(r io.Reader) (io.ReadCloser, error) {
			return &reader{snappy.NewReader(r)}, nil
		},
		func(w io.Writer) (io.WriteCloser, error) {
			return snappy.NewBufferedWriter(w), nil
		},
	)
}

/////////////////////////////////////////////////////////////////////////
// reader functions
/////////////////////////////////////////////////////////////////////////

// Close is a no-op and always returns nil.
func (*reader) Close() error {
	return nil
}
