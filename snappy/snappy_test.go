// Snappy_test provides tests for the Snappy compressor.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package snappy

import (
	"bitbucket.org/pcastools/compress"
	"bitbucket.org/pcastools/compress/internal/compresstest"
	"testing"
)

func TestSnappy(t *testing.T) {
	compresstest.TestType(compress.Snappy, t)
}
