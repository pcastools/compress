// None provides the None compression type (i.e. no compression).

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package compress

import (
	"io"
	"io/ioutil"
)

// writer wraps an io.Writer and provides a no-op Close method.
type writer struct {
	io.Writer
}

/////////////////////////////////////////////////////////////////////////
// Local functions
/////////////////////////////////////////////////////////////////////////

// init registers the Snappy compression format.
func init() {
	Register(
		None,
		func(r io.Reader) (io.ReadCloser, error) {
			return ioutil.NopCloser(r), nil
		},
		func(w io.Writer) (io.WriteCloser, error) {
			return &writer{w}, nil
		},
	)
}

/////////////////////////////////////////////////////////////////////////
// writer functions
/////////////////////////////////////////////////////////////////////////

// Close is a no-op and always returns nil.
func (*writer) Close() error {
	return nil
}
